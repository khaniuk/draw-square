# Draw Square

Ejercicio: Dado un número dibujar un cuadrado con el caracter \* en los laterales.

## Install dependencies

```
yarn install
```

## Run script

```
node src/app
```

## Run test

```
yarn test

yarn test:watch
```
