const square = (row, col, num) => {
  if (row === 0 || col === 0) {
    return "*";
  }
  if (row === num || col === num) {
    return "*";
  }
  return " ";
};

const main = (num) => {
  if (num < 3) {
    console.log("Please enter a number > 2");
    return false;
  }

  for (let row = 0; row < num; row++) {
    let element = "";
    for (let col = 0; col < num; col++) {
      element += square(row, col, num - 1);
    }
    console.log(element);
  }
};

main(5);

module.exports = { square, main };
