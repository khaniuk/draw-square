const { square, main } = require("../src/app");

describe("draw-square", () => {
  test("Print * if they receive 0, 0, N", () => {
    const expected = "*";
    const result = square(0, 0, 3);
    expect(expected).toBe(result);
  });

  test("Print * if they receive 0, 1, N", () => {
    const expected = "*";
    const result = square(0, 1, 3);
    expect(expected).toBe(result);
  });

  test("Print * if they receive 1, 0, N", () => {
    const expected = "*";
    const result = square(1, 0, 3);
    expect(expected).toBe(result);
  });

  test("Print * if they receive N, 0, N", () => {
    const expected = "*";
    const result = square(3, 0, 3);
    expect(expected).toBe(result);
  });

  test("Print * if they receive 0, N, N", () => {
    const expected = "*";
    const result = square(0, 3, 3);
    expect(expected).toBe(result);
  });

  test("Print * if they receive N, N, N", () => {
    const expected = "*";
    const result = square(3, 3, 3);
    expect(expected).toBe(result);
  });
});

describe("function main", () => {
  test("should fail if the length of the square is less than 2", () => {
    const expected = false;
    const result = main(2);
    expect(expected).toBeFalsy();
  });
});
